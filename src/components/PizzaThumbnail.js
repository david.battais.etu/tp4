import Component from './Component.js';
import Img from './Img.js';

export default class PizzaThumbnail extends Component {
	constructor({ name, image, price_small, price_large }) {
		super('article', { name: 'class', value: 'pizzaThumbnail' }, [
			new Component('a', { name: 'href', value: image }, [
				new Img(image),
				new Component('section', null, [
					new Component('h4', null, name),
					new Component('ul', null, [
						new Component(
							'li',
							null,
							`Prix petit format : ${price_small.toFixed(2)} €`
						),
						new Component(
							'li',
							null,
							`Prix grand format : ${price_large.toFixed(2)} €`
						),
					]),
				]),
			]),
		]);
	}
	mount(element) {
		super.mount(element);
		console.log('non');
		fetch('http://localhost:8080/api/v1/pizzas')
			.then(response => response.text())
			.then(console.log('oui'));
	}
}
